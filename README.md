# Learn-PY-ID

## Author
Baha ID "bahaxid@gmail.com"

## Introduction
Repository ini berisi kumpulan contoh kode dalam bahasa Python.

## Referensi
referensi example:
https://codingcompiler.com/python-coding-interview-questions-answers/

download library di:
https://www.lfd.uci.edu/~gohlke/pythonlibs/

### plot
https://stackoverflow.com/questions/18973404/setting-different-bar-color-in-matplotlib-python
https://stackoverflow.com/questions/30802688/how-can-i-resample-roc-curve-fpr-tpr
https://solarianprogrammer.com/2017/02/25/install-numpy-scipy-matplotlib-python-3-windows/
https://python-graph-gallery.com/392-use-faceting-for-radar-chart/

### parallel
https://sebastianraschka.com/Articles/2014_multiprocessing.html
https://medium.com/apteo/multi-processing-in-python-ee0ce73a459b
https://blog.dominodatalab.com/simple-parallelization/
https://stackoverflow.com/questions/33787678/writing-a-parallel-loop
https://medium.com/@urban_institute/using-multiprocessing-to-make-python-code-faster-23ea5ef996ba

### Machine Learning
http://dataaspirant.com/2017/02/01/decision-tree-algorithm-python-with-scikit-learn/
https://pythonprogramminglanguage.com/kmeans-clustering-centroid/
https://pythonprogramminglanguage.com
https://pythonprogramminglanguage.com/machine-learning-classifier/

### List
https://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks https://stackoverflow.com/questions/176918/finding-the-index-of-an-item-given-a-list-containing-it-in-python#17202481
https://www.programiz.com/python-programming/methods/list/sort
